package com.ent.printer;

import java.io.FileWriter;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;

public class FilePrinter {
	
	private FilePrinter() {
	}
		
	public static String convertObjectToXml(Object object) {
		String xml = "";
		ObjectMapper mapper = new XmlMapper();
		AnnotationIntrospector introspector = new JaxbAnnotationIntrospector(mapper.getTypeFactory()); 
		mapper.setAnnotationIntrospector(introspector);
		try {
			xml = mapper.writeValueAsString(object);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return xml;
	}
	
	public static void generateFile(String fileName, String xml) {
		try (FileWriter fw = new FileWriter(fileName + ".xml")) {
			xml = getPrettyString(xml, 2);
		    fw.write(xml);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static String getPrettyString(String xmlData, int indent) throws Exception {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		transformerFactory.setAttribute("indent-number", indent);

		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		StringWriter stringWriter = new StringWriter();
		StreamResult xmlOutput = new StreamResult(stringWriter);

		Source xmlInput = new StreamSource(new StringReader(xmlData));
		transformer.transform(xmlInput, xmlOutput);

		return xmlOutput.getWriter().toString();
	}
}
