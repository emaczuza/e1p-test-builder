package com.ent.builder;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.cgi.ratabase.Coverage;
import com.cgi.ratabase.Coverages;
import com.cgi.ratabase.Item;

public class TestCase {
	private String caseId;
	private Map<String, String> policyFields;
	private Map<String, String> policyInputFields;
	private Map<String, String> policyOutputFields;
	private Map<String, ArrayList<String>> policyInputArrayFields; 
	private Map<String, Integer> policyInputArraySizes;
	private Map<String, Integer> policyOutputArray2DSizes;
	private List<Item> items;
	private Coverages coverages;
	private	String state;
	private String program;
	
	public TestCase() {
		policyFields = new LinkedHashMap<>();
		policyInputFields = new LinkedHashMap<>();
		policyOutputFields = new LinkedHashMap<>();
		policyInputArrayFields = new LinkedHashMap<>();
		policyInputArraySizes = new LinkedHashMap<>();
		policyOutputArray2DSizes = new LinkedHashMap<>();
		items = new ArrayList<>();
		coverages = new Coverages();
		state = null;
		program = null;
	}
	
	public String getCaseId() {
		return caseId;
	}
	
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	// policyFields
	
	public Map<String, String> getPolicyFields() {
		return policyFields;
	}
	
	public void setPolicyFields(Map<String, String> fields) {
		this.policyFields = fields;
	}
	
	public String putPolicyFields(String key, String value) {
		return policyFields.put(key, value);
	}
	
	public String getPolicyFields(String key) {
		return policyFields.get(key);
	}

	// policyInputFields

	public Map<String, String> getPolicyInputFields() {
		return policyInputFields;
	}
	
	public void setPolicyInputFields(Map<String, String> fields) {
		this.policyInputFields = fields;
	}
	
	public String putPolicyInputFields(String key, String value) {
		return policyInputFields.put(key, value);
	}
	
	public String getPolicyInputFields(String key) {
		return policyInputFields.get(key);
	}

	// policyOutputFields

	public Map<String, String> getPolicyOutputFields() {
		return policyOutputFields;
	}
	
	public void setPolicyOutputFields(Map<String, String> fields) {
		this.policyOutputFields = fields;
	}
	
	public String putPolicyOutputFields(String key, String value) {
		return policyOutputFields.put(key, value);
	}
	
	public String getPolicyOutputFields(String key) {
		return policyOutputFields.get(key);
	}
	
	// policyInputArrayFields
	
	public Map<String, ArrayList<String>> getPolicyInputArrayFields() {
		return policyInputArrayFields;
	}
	
	public void setPolicyInputArrayFields(Map<String, ArrayList<String>> fields) {
		this.policyInputArrayFields = fields;
	}
	
	public ArrayList<String> putPolicyInputArrayFields(String key, String value) {
		ArrayList<String> list = policyInputArrayFields.get(key);
		
		if (list == null) {
			list = new ArrayList<String>();
		}
		
		list.add(value);
		
		return policyInputArrayFields.put(key, list);
	}
	
	public ArrayList<String> getPolicyInputArrayFields(String key) {
		return policyInputArrayFields.get(key);
	}

	// policyInputArraySizes
	
	public Map<String, Integer> getPolicyInputArraySizes() {
		return policyInputArraySizes;
	}
	
	public void setPolicyInputArraySizes(Map<String, Integer> fields) {
		this.policyInputArraySizes = fields;
	}
	
	public Integer putPolicyInputArraySizes(String key, Integer value) {
		return policyInputArraySizes.put(key, value);
	}
	
	public Integer getPolicyInputArraySizes(String key) {
		return policyInputArraySizes.get(key);
	}

	// policyOutputArray2DSizes
	
	public Map<String, Integer> getPolicyOutputArray2DSizes() {
		return policyOutputArray2DSizes;
	}
	
	public void setPolicyOutputArray2DSizes(Map<String, Integer> fields) {
		this.policyOutputArray2DSizes = fields;
	}
	
	public Integer putPolicyOutputArray2DSizes(String key, Integer value) {
		return policyOutputArray2DSizes.put(key, value);
	}
	
	public Integer getPolicyOutputArray2DSizes(String key) {
		return policyOutputArray2DSizes.get(key);
	}

	// items
	
	public List<Item> getItems() {
		return items;
	}
	
	public void setItems(List<Item> items) {
		this.items = items;
	}
	
	public boolean putItems(Item item) {
		return items.add(item);
	}
	
	public Item getItems(Integer index) {
		return items.get(index);
	}

	// coverages

	public Coverages getCoverages() {
		return coverages;
	}
	
	public void setCoverages(Coverages coverages) {
		this.coverages = coverages;
	}
	
	public boolean putCoverages(Coverage coverage) {
		return coverages.getCoverage().add(coverage);
	}
	
	public Coverage getCoverages(Integer index) {
		return coverages.getCoverage().get(index);
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getProgram() {
		return program;
	}

	public void setProgram(String program) {
		this.program = program;
	}


}
