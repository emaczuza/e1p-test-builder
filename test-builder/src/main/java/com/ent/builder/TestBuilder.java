package com.ent.builder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cgi.ratabase.Coverage;
import com.cgi.ratabase.Item;
import com.cgi.ratabase.RatabaseCalcArrayValueList;
import com.cgi.ratabase.RatabaseFieldArray;
import com.cgi.ratabase.RatabaseFieldArray2D;
import com.cgi.ratabase.RatabaseFieldArray2DRowList;
import com.cgi.ratabase.RatabaseFieldArrayList;
import com.cgi.ratabase.RatabaseFieldArrayList2D;
import com.cgi.ratabase.RatabaseFieldList;
import com.cgi.ratabase.RatabaseItemCovRefID;
import com.cgi.ratabase.RatingServiceObjectsRequest;
import com.ent.printer.FilePrinter;
import com.ent.utils.RateConstants;
import com.ent.utils.RestClient;

import ent.rate.builder.auto.RateRequestBuilderUtil;

public class TestBuilder {
	
	private TestBuilder() {}

	private static HashMap<String, ArrayList<String>> itemsMap = new HashMap<>();
	private static HashMap<String, ArrayList<String>> coveragesMap = new HashMap<>();
	private static ArrayList<TestCase> testCases = new ArrayList<>();

	public static void build(String excelFilePath, String destinationFolderName) throws IOException {

		System.out.println("Starting parsing input.");

		parseExcel(excelFilePath);
		
		System.out.println("Finished parsing input. Generating request files.");
				
		writeFiles(destinationFolderName);
						
		System.out.println("Finished outputting generated files to: " + destinationFolderName);
	}
	
	private static void parseExcel(String excelFilePath) {
		ArrayList<String> mainTypes = new ArrayList<>();
		ArrayList<String> mainNames = new ArrayList<>();
		ArrayList<String> mainPositions = new ArrayList<>();

		ArrayList<String> itemsTypes = new ArrayList<>();
		ArrayList<String> itemsNames = new ArrayList<>();
		ArrayList<String> itemsPositions = new ArrayList<>();

		ArrayList<String> coveragesTypes = new ArrayList<>();
		ArrayList<String> coveragesNames = new ArrayList<>();
		ArrayList<String> coveragesPositions = new ArrayList<>();

		try {
			FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
			XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
			Iterator<Sheet> worksheetIterator = workbook.iterator();
			
			int numberOfSheets = workbook.getNumberOfSheets();

			for (int i = (numberOfSheets - 1); i >= 0; i--) {

				Sheet sheet = workbook.getSheetAt(i);
				System.out.println(sheet.getSheetName());

				int rowStart = Math.min(15, sheet.getFirstRowNum());
				int rowEnd = Math.max(1400, sheet.getLastRowNum());

				for (int rowNum = rowStart; rowNum < rowEnd; rowNum++) {
					ArrayList<String> rowList = new ArrayList<>();

					Row row = sheet.getRow(rowNum);

					if (row == null) {
						// This whole row is empty
						continue;
					}

					int lastColumn = Math.max(row.getLastCellNum(), 5);

					String mainLastName = "";

					String mainLastType = "";
					String mainValue = "";
					String mainIndex = "";
					Integer mainSize = null;
					
					TestCase test = new TestCase();

					for (int cn = 0; cn < lastColumn; cn++) {
						String mainType = "";
						String mainName = "";

						Cell cell = row.getCell(cn);

						if (sheet.getSheetName().equals("Coverages")) {

							switch (row.getRowNum()) {
							case 0:
								coveragesTypes.add(readCellValue(cell));
								break;
							case 1:
								coveragesNames.add(readCellValue(cell));
								break;
							case 2:
								coveragesPositions.add(readCellValue(cell));
								break;
							default:
								rowList.add(readCellValue(cell));
								break;
							}

						}

						if (sheet.getSheetName().equals("Items")) {

							switch (row.getRowNum()) {
							case 0:
								itemsTypes.add(readCellValue(cell));
								break;
							case 1:
								itemsNames.add(readCellValue(cell));
								break;
							case 2:
								itemsPositions.add(readCellValue(cell));
								break;
							default:
								rowList.add(readCellValue(cell));
								break;
							}

						}

						if (sheet.getSheetName().equals("Main")) {

							switch (row.getRowNum()) {
							case 0:
								mainTypes.add(readCellValue(cell));
								break;
							case 1:
								mainNames.add(readCellValue(cell));
								break;
							case 2:
								mainPositions.add(readCellValue(cell));
								break;
							default:

								Integer start = null;
								Integer end = null;
								Integer x2D = null;
								Integer y2D = null;
								
								if (mainTypes.get(cn) != null) {
									mainType = mainTypes.get(cn).trim();
									mainLastType = mainType;
								}

								if (mainType.isEmpty()) {
									mainType = mainLastType;
								}

								switch (mainType) {
								case "TestCaseID":
									test.setCaseId(readCellValue(cell));
									break;
								case "Policy":
									mainName = mainNames.get(cn).trim();
									mainValue = readCellValue(cell);
									
									if (mainName.equals("State")) {
										test.setState(mainValue);
									} else if (mainName.equals("Program")) {
										test.setProgram(mainValue);
									} else {
										test.putPolicyFields(mainName, mainValue);
									}

									break;
								case "Policy.Inputs":
								case "Policy.Outputs": {
									mainName = mainNames.get(cn).trim();
									mainValue = readCellValue(cell);
									
									if (mainType.equals("Policy.Inputs")) {
										test.putPolicyInputFields(mainName, mainValue);
									} else {
										test.putPolicyOutputFields(mainName, mainValue);
									}
									
									break;
								}
								case "Policy.InputsArray":
									if (mainNames.get(cn) == null) {
										mainName = "";
									} else {
										mainName = mainNames.get(cn).trim();
									}

									if (mainName.isEmpty()) {
										mainName = mainLastName;
									}

									mainLastName = mainName;

									mainValue = readCellValue(cell);

									start = mainName.indexOf("(") + 1;
									end = mainName.indexOf(")");

									mainSize = Integer.parseInt(mainName.substring(start, end));

									mainName = mainName.substring(0, (start - 2)).trim();

									test.putPolicyInputArraySizes(mainName, mainSize);
									test.putPolicyInputArrayFields(mainName, mainValue);

									break;
								case "Policy.OutputsArray2D":
									if (mainNames.get(cn) == null) {
										mainName = null;
									} else {
										mainName = mainNames.get(cn).trim();
									}

									if (mainName == null) {
										mainName = mainLastName;
									}

									mainLastName = mainName;

									start = mainName.indexOf("(") + 1;
									end = mainName.indexOf(")");

									mainSize = Integer.parseInt(mainName.substring(start, end));

									mainName = mainName.substring(0, (start - 2)).trim();

									test.putPolicyOutputArray2DSizes(mainName, mainSize);

									break;
								case "Item":
									mainValue = readCellValue(cell);

									RatabaseFieldList itemInputs = new RatabaseFieldList();
									RatabaseFieldList itemOutputs = new RatabaseFieldList();
									Item item = new Item();
									
									item.setInputsArray(new RatabaseFieldArrayList());
									item.setInputsArray2D(new RatabaseFieldArrayList2D());
									item.setOutputsArray2D(new RatabaseFieldArrayList2D());
									
									RatabaseFieldArray fieldArray = new RatabaseFieldArray();
									RatabaseFieldArray2D input2D = new RatabaseFieldArray2D();

									com.cgi.ratabase.Row tempRow = new com.cgi.ratabase.Row();

									ArrayList<String> itemList = new ArrayList<String>();

									String itemType = null;
									String itemLastType = null;
									String itemName = null;
									String itemLastName = null;
									String itemValue = null;
									String itemIndex = null;
									Integer itemSize = null;
									String xValue = null;
									Integer row2dNum = 1;

									// System.out.println(mainValue);

									itemList = itemsMap.get(mainValue);

									for (int itemNum = 1; itemNum < itemsTypes.size(); itemNum++) {

										if (itemNum > (itemsTypes.size() - 1)) {
											itemType = null;
										} else {
											if (itemsTypes.get(itemNum) == null) {
												itemType = null;
											} else {
												itemType = itemsTypes.get(itemNum).trim();
											}
										}

										if (itemType == null) {
											itemType = itemLastType;
										}

										if (itemLastType != null && fieldArray != null) {
											if (itemLastType.equals("Item.InputsArray")
													&& !itemType.equals("Item.InputsArray")) {
												fieldArray.getValue().getString().add("");
												item.getInputsArray().getRatabaseFieldArray().add(fieldArray);
											}
											
											if (itemLastType.equals("Item.InputsArray2D")
													&& !itemType.equals("Item.InputsArray2D")) {
												tempRow = RequestBuilder.populateRow(row2dNum, null, null);
												input2D.getRows().getRow().add(tempRow);
												
												item.getInputsArray2D().getRatabaseFieldArray2D().add(input2D);
												row2dNum = 1;
												input2D = null;
											}
										}
										
										itemLastType = itemType;

										// System.out.println(itemNum + " " + itemType);

										switch (itemType) {
										case "Item.Inputs":
										case "Item.Outputs": {
											itemName = itemsNames.get(itemNum).trim();
											itemValue = itemList.get(itemNum);

											if (itemType.equals("Item.Inputs")) {
												itemInputs.getRatabaseField().add(RateRequestBuilderUtil.populateAttribute(itemName, itemValue));
											} else {
												itemOutputs.getRatabaseField().add(RateRequestBuilderUtil.populateAttribute(itemName, itemValue));
											}

											break;
										}
										case "Item.ItemCovRefID":
											itemValue = itemList.get(itemNum);

											if (itemValue != null) {
												// System.out.println(itemValue);

												RatabaseFieldList covInputs = new RatabaseFieldList();
												RatabaseFieldList covOutputs = new RatabaseFieldList();
												Coverage coverage = new Coverage();

												coverage.setInputsArray2D(new RatabaseFieldArrayList2D());
												coverage.setOutputsArray2D(new RatabaseFieldArrayList2D());

												ArrayList<String> covList = new ArrayList<String>();

												String covType = null;
												String covLastType = null;
												String covName = null;
												String covLastName = null;
												String covValue = null;
												String covIndex = null;
												Integer covSize = null;

												covList = coveragesMap.get(itemValue);

												String covId = UUID.randomUUID().toString();

												if (item.getItemCovRefID() == null) {
													item.setItemCovRefID(new RatabaseItemCovRefID());
												}
												item.getItemCovRefID().getString().add(covId);
												
												coverage.setCoverageID(covId);

												for (int covNum = 1; covNum < coveragesNames.size(); covNum++) {

													if (covNum > (coveragesTypes.size() - 1)) {
														covType = null;
													} else {
														if (coveragesTypes.get(covNum) == null) {
															covType = null;
														} else {
															covType = coveragesTypes.get(covNum).trim();
														}
													}

													if (covType == null) {
														covType = covLastType;
													}

													if (covLastType != null && input2D != null) {
														if (covLastType.equals("Coverage.InputsArray2D")
																&& !covType.equals("Coverage.InputsArray2D")) {
															tempRow = RequestBuilder.populateRow(row2dNum, null, null);
															input2D.getRows().getRow().add(tempRow);
															
															coverage.getInputsArray2D().getRatabaseFieldArray2D().add(input2D);
															row2dNum = 1;
															input2D = null;
														}
													}
													
													covLastType = covType;

													switch (covType) {
													case "FormulaName":
														coverage.setFormulaName(covList.get(covNum));
														
														break;
													case "Coverage.Inputs":
													case "Coverage.Outputs": {
														covName = coveragesNames.get(covNum).trim();
														covValue = covList.get(covNum);

														if (covType.equals("Coverage.Inputs")) {
															covInputs.getRatabaseField().add(RateRequestBuilderUtil.populateAttribute(covName, covValue));
														} else {
															covOutputs.getRatabaseField().add(RateRequestBuilderUtil.populateAttribute(covName, covValue));
														}

														break;
													}
													case "Coverage.InputsArray2D":
													case "Coverage.OutputsArray2D": {

														if (covNum > (coveragesNames.size() - 1)) {
															covName = null;
														} else {
															if (coveragesNames.get(covNum) == null) {
																covName = null;
															} else {
																covName = coveragesNames.get(covNum).trim();
															}
														}

														if (covName == null) {
															covName = covLastName;
														}

														covLastName = covName;

														if (covNum > (covList.size() - 1)) {
															covValue = null;
														} else {
															covValue = covList.get(covNum);
														}

														start = covName.indexOf("(") + 1;
														end = covName.indexOf(")");

														covSize = Integer.parseInt(covName.substring(start, end));

														covName = covName.substring(0, (start - 2)).trim();

														if (coveragesPositions.get(covNum) != null) {
															covIndex = coveragesPositions.get(covNum).trim();
															start = covIndex.indexOf(",") + 1;

															x2D = Integer.parseInt(covIndex.substring(0, (start - 1)));
															y2D = Integer.parseInt(covIndex.substring(start));
														}

														if (covType.equals("Coverage.InputsArray2D")) {
															if (x2D == 1 && y2D == 1) {
																input2D = new RatabaseFieldArray2D();

																input2D.setName(covName);
																input2D.setSize(covSize);
																input2D.setRows(new RatabaseFieldArray2DRowList());
															}
															
															if (y2D == 1) {
																xValue = covValue;
															} else {
																tempRow = RequestBuilder.populateRow(row2dNum, xValue, covValue);
																input2D.getRows().getRow().add(tempRow);
																row2dNum++;
															}
														} else {
															coverage.getOutputsArray2D().getRatabaseFieldArray2D().add(RequestBuilder.populate2DArray(covName, covSize));
														}

														break;
													}
													}

												}

												coverage.setInputs(covInputs);
												coverage.setOutputs(covOutputs);
												
												test.putCoverages(coverage);
											}

											break;
										case "Item.InputsArray":
											if (itemsNames.get(itemNum) == null) {
												itemName = null;
											} else {
												itemName = itemsNames.get(itemNum).trim();
											}

											if (itemName == null) {
												itemName = itemLastName;
											}

											itemLastName = itemName;

											itemValue = itemList.get(itemNum);
											itemIndex = itemsPositions.get(itemNum).trim();

											start = itemName.indexOf("(") + 1;
											end = itemName.indexOf(")");

											itemSize = Integer.parseInt(itemName.substring(start, end));

											itemName = itemName.substring(0, (start - 2)).trim();

											if (itemIndex.equals("1")) {
												fieldArray = new RatabaseFieldArray();
												
												fieldArray.setName(itemName);
												fieldArray.setSize(itemSize);
												fieldArray.setValue(new RatabaseCalcArrayValueList());

												fieldArray.getValue().getString().add(itemValue);
											} else if (Integer.parseInt(itemIndex) == itemSize) {
												fieldArray.getValue().getString().add(itemValue);
												
												item.getInputsArray().getRatabaseFieldArray().add(fieldArray);
												
												fieldArray = null;
											} else {
												fieldArray.getValue().getString().add(itemValue);
											}
											
											break;
										case "Item.InputsArray2D":
										case "Item.OutputsArray2D": {

											if (itemNum > (itemsNames.size() - 1)) {
												itemName = null;
											} else {
												if (itemsNames.get(itemNum) == null) {
													itemName = null;
												} else {
													itemName = itemsNames.get(itemNum).trim();
												}
											}

											if (itemName == null) {
												itemName = itemLastName;
											}

											itemLastName = itemName;
											
											if (itemNum > (itemList.size() - 1)) {
												itemValue = null;
											} else {
												itemValue = itemList.get(itemNum);
											}

											start = itemName.indexOf("(") + 1;
											end = itemName.indexOf(")");

											itemSize = Integer.parseInt(itemName.substring(start, end));

											itemName = itemName.substring(0, (start - 2)).trim();

											if (itemNum < (itemList.size() - 1)) {
												if (itemsPositions.get(itemNum) != null) {
													itemIndex = itemsPositions.get(itemNum).trim();
													start = itemIndex.indexOf(",") + 1;

													x2D = Integer.parseInt(itemIndex.substring(0, (start - 1)));
													y2D = Integer.parseInt(itemIndex.substring(start));
												}
											}

											if (itemType.equals("Item.InputsArray2D")) {
												if (x2D == 1 && y2D == 1) {
													input2D = new RatabaseFieldArray2D();

													input2D.setName(itemName);
													input2D.setSize(itemSize);
													input2D.setRows(new RatabaseFieldArray2DRowList());
												}
												
												if (y2D == 1) {
													xValue = itemValue;
												} else {
													tempRow = RequestBuilder.populateRow(row2dNum, xValue, itemValue);
													input2D.getRows().getRow().add(tempRow);
													row2dNum++;
												}
											} else {
												item.getOutputsArray2D().getRatabaseFieldArray2D().add(RequestBuilder.populate2DArray(itemName, itemSize));
											}

											break;
										}
										}
									}
									
									item.setInputs(itemInputs);
									item.setOutputs(itemOutputs);
									
									test.putItems(item);
									
									break;
								}
								break;
							}
						}
					}

					if (sheet.getSheetName().equals("Items") && rowList.size() > 0) {
						itemsMap.put(rowList.get(0), rowList);
					}

					if (sheet.getSheetName().equals("Coverages") && rowList.size() > 0) {
						coveragesMap.put(rowList.get(0), rowList);
					}
					
					if (test.getCaseId() != null && !test.getCaseId().isEmpty()) {
						testCases.add(test);
					}
				}
			}

			workbook.close();
			inputStream.close();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String readCellValue(Cell checkCell) {
		if (checkCell == null || checkCell.getCellType() == CellType.BLANK) {
			return null;
		}

		DataFormatter df = new DataFormatter();
		return df.formatCellValue(checkCell);
	}
	
	private static void writeFiles(String destinationFolderName) {
		for (TestCase testCase : testCases) {
			RatingServiceObjectsRequest request = RequestBuilder.generateRequest(testCase);
			String requestString = FilePrinter.convertObjectToXml(request);
			String responseString = RestClient.callRatabase(requestString);
			FilePrinter.generateFile(destinationFolderName + "responses\\" + testCase.getCaseId(), responseString);
			FilePrinter.generateFile(destinationFolderName + "requests\\" + testCase.getCaseId(), RateConstants.REQUEST_HEADER + requestString + RateConstants.REQUEST_FOOTER);
		}
	}

}
