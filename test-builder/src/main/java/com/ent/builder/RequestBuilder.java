package com.ent.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.cgi.ratabase.Col;
import com.cgi.ratabase.Coverages;
import com.cgi.ratabase.Item;
import com.cgi.ratabase.Items;
import com.cgi.ratabase.LOB;
import com.cgi.ratabase.LOBS;
import com.cgi.ratabase.Policies;
import com.cgi.ratabase.Policy;
import com.cgi.ratabase.RatabaseCalcArrayValueList;
import com.cgi.ratabase.RatabaseField;
import com.cgi.ratabase.RatabaseFieldArray;
import com.cgi.ratabase.RatabaseFieldArray2D;
import com.cgi.ratabase.RatabaseFieldArray2DColList;
import com.cgi.ratabase.RatabaseFieldArray2DRowList;
import com.cgi.ratabase.RatabaseFieldArrayList;
import com.cgi.ratabase.RatabaseFieldArrayList2D;
import com.cgi.ratabase.RatabaseFieldList;
import com.cgi.ratabase.RatabaseProperties;
import com.cgi.ratabase.RatingServiceObjectsRequest;
import com.cgi.ratabase.Region;
import com.cgi.ratabase.Regions;
import com.cgi.ratabase.Row;
import com.cgi.ratabase.ServiceProperties;

public  class RequestBuilder {
	
	private RequestBuilder() {}

	public static RatingServiceObjectsRequest generateRequest(TestCase testCase) {
		RatingServiceObjectsRequest engineRequest = new RatingServiceObjectsRequest();
		engineRequest.setRatabaseProperties(new RatabaseProperties());
		engineRequest.getRatabaseProperties().setPolicies(new Policies());
		engineRequest.getRatabaseProperties().getPolicies().getPolicy().add(createPolicy(testCase));
		
		engineRequest.setServiceProperties(new ServiceProperties());
		engineRequest.getServiceProperties().setServiceId("synthetic" + testCase.getCaseId());
		
		return engineRequest;
	}

	private static Policy createPolicy(TestCase testCase) {
		Policy policy = new Policy();

		policy.setDefaultCompanyEffectiveDate(testCase.getPolicyFields("DefaultCompanyEffectiveDate"));
		policy.setDefaultRetrievalDate(testCase.getPolicyFields("DefaultRetrievalDate"));
		policy.setLegalEntityName(testCase.getPolicyFields("LegalEntityName"));
		policy.setLegalEntityOwner(testCase.getPolicyFields("LegalEntityOwner"));
		policy.setBusinessDes("N");
		
		// Inputs
		policy.setInputs(new RatabaseFieldList());
		policy.getInputs().getRatabaseField().addAll(createRatabaseFieldList(testCase.getPolicyInputFields()));
		
		// InputsArray
		policy.setInputsArray(new RatabaseFieldArrayList());
		policy.setInputsArray(createRatabaseArrayList(testCase.getPolicyInputArrayFields(), testCase.getPolicyInputArraySizes()));

		// Outputs
		policy.setOutputs(new RatabaseFieldList());
		policy.getOutputs().getRatabaseField().addAll(createRatabaseFieldList(testCase.getPolicyOutputFields()));

		// OutputsArray2D
		policy.setOutputsArray2D(new RatabaseFieldArrayList2D());

		for (Entry<String, Integer> entry : testCase.getPolicyOutputArray2DSizes().entrySet()) {
			policy.getOutputsArray2D().getRatabaseFieldArray2D().add(populate2DArray(entry.getKey(), entry.getValue()));
		}

		policy.setLOBS(populateLOB(testCase.getItems(), testCase.getCoverages(), testCase.getProgram()));
		
		policy.getLOBS().getLOB().get(0).setLegalEntityProductGroup(testCase.getState());
		
		return policy;
	}

	private static List<RatabaseField> createRatabaseFieldList(Map<String, String> fields) {
		List<RatabaseField> fieldList = new ArrayList<>();
		
		for (Map.Entry<String, String> entry : fields.entrySet()) {
			RatabaseField field = new RatabaseField();
			field.setName(entry.getKey());
			field.setValue(entry.getValue());
			fieldList.add(field);
		}
		
		return fieldList;
	}

	private static RatabaseFieldArrayList createRatabaseArrayList(Map<String, ArrayList<String>> fields, Map<String, Integer> fieldSizes) {	
		RatabaseFieldArrayList inputArray = new RatabaseFieldArrayList();
		
		for (Map.Entry<String, ArrayList<String>> entry : fields.entrySet()) {
			RatabaseFieldArray fieldArray = new RatabaseFieldArray();

			fieldArray.setName(entry.getKey());
			fieldArray.setSize(fieldSizes.get(entry.getKey()));
			fieldArray.setValue(new RatabaseCalcArrayValueList());
			
			fieldArray.getValue().getString().addAll(entry.getValue());
			fieldArray.getValue().getString().add("");

			inputArray.getRatabaseFieldArray().add(fieldArray);
		}
		
		return inputArray;
	}

	public static RatabaseFieldArray2D populate2DArray(String name, Integer size) {
		RatabaseFieldArray2D output2D = new RatabaseFieldArray2D();
		output2D.setName(name);
		output2D.setSize(size);
		output2D.setRows(new RatabaseFieldArray2DRowList());

		for (int i = 1; i <= size; i++) {
			Row tempRow = populateRow(i, null, "0");
			output2D.getRows().getRow().add(tempRow);
		}
		return output2D;
	}

	public static Row populateRow(Integer rowNumber, String key, String value) {
		Row row = new Row();
		row.setLabel(rowNumber.toString());
		row.setCols(new RatabaseFieldArray2DColList());

		Col keyCol = new Col();
		keyCol.setLabel("1");
		keyCol.setValue(key);

		Col valueCol = new Col();
		valueCol.setLabel("2");
		valueCol.setValue(value);

		row.getCols().getCol().add(keyCol);
		row.getCols().getCol().add(valueCol);

		return row;
	}

	static LOBS populateLOB(List<Item> itemsArray, Coverages coverages, String program) {

		LOBS linesOfBusinessParent = new LOBS();
		LOB lineOfBusiness = new LOB();
		Regions regionsParent = new Regions();
		Region region = new Region();

		region.setLegalEntityRegion(program);
		
		if (coverages != null) {
			region.setCoverages(coverages);	
		}

		if(itemsArray != null && !itemsArray.isEmpty()) {
			Items items = new Items();
			items.getItem().addAll(itemsArray);
			region.setItems(items);				
		}
		regionsParent.getRegion().add(region);
		lineOfBusiness.setRegions(regionsParent);
		linesOfBusinessParent.getLOB().add(lineOfBusiness);

		return linesOfBusinessParent;
	}
	
}
