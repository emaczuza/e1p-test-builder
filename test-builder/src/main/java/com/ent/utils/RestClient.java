package com.ent.utils;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

public class RestClient {

	private static CloseableHttpClient classClient;

	private RestClient() {}
	
	public static String callRatabase(String request) {
		HttpClient client = getHttpClient();
		try {
			HttpPost engineRequest = new HttpPost("https://rateplatformservice-dev01.sbx.ent-rating-01.aws.ent.corp/rateplatformservice/rate/engine/auto");
			engineRequest.addHeader("Content-Type", "application/xml");
			engineRequest.setEntity(new StringEntity(request, ContentType.APPLICATION_XML));
			HttpResponse response = client.execute(engineRequest);
			return EntityUtils.toString(response.getEntity());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static CloseableHttpClient getHttpClient() {
		if (classClient == null) {
			classClient = getCloseableHttpClientWithSSLOff();
		}
		
		return classClient;
	}

	private static CloseableHttpClient getCloseableHttpClientWithSSLOff() {
		CloseableHttpClient httpClient = null;
		try {
			httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
					.setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
						public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
							return true;
						}
					}).build()).build();
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			System.out.println("Error creating http client instance " + e);
		}

		return httpClient;
	}
}
