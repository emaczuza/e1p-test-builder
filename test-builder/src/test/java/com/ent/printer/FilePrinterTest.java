package com.ent.printer;

import static org.junit.Assert.assertNotEquals;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

import com.cgi.ratabase.Coverage;
import com.cgi.ratabase.Coverages;
import com.cgi.ratabase.Item;
import com.cgi.ratabase.Items;
import com.cgi.ratabase.LOB;
import com.cgi.ratabase.LOBS;
import com.cgi.ratabase.Policies;
import com.cgi.ratabase.Policy;
import com.cgi.ratabase.RatabaseField;
import com.cgi.ratabase.RatabaseFieldList;
import com.cgi.ratabase.RatabaseProperties;
import com.cgi.ratabase.RatingServiceObjectsRequest;
import com.cgi.ratabase.Region;
import com.cgi.ratabase.Regions;

public class FilePrinterTest {
	
	@Test
	public void generateFile_test() {
		RatingServiceObjectsRequest engineRequest = generateTestRequest();
		String xml = FilePrinter.convertObjectToXml(engineRequest);
		FilePrinter.generateFile("test", xml);
		String output = "";
		try (BufferedReader br = new BufferedReader(new FileReader("test.xml"))) {
			output = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertNotEquals("", output);
	}

	private RatingServiceObjectsRequest generateTestRequest() {
		RatingServiceObjectsRequest engineRequest = new RatingServiceObjectsRequest();
		engineRequest.setRatabaseProperties(new RatabaseProperties());
		engineRequest.getRatabaseProperties().setPolicies(new Policies());
		engineRequest.getRatabaseProperties().getPolicies().getPolicy().add(createPolicy());
		
		return engineRequest;
	}
	
	private Policy createPolicy() {
		Policy policy = new Policy();
		
		LOBS lobs01 = new LOBS();

		policy.setLOBS(lobs01);
		List<LOB> lobList = lobs01.getLOB();
		LOB lob01 = new LOB();
		lobList.add(lob01);

		Regions regions01 = new Regions();
		lob01.setRegions(regions01);
		List<Region> regionList = regions01.getRegion();

		Region region01 = new Region();
		regionList.add(region01);

		Items items = new Items();
		region01.setItems(items);

		List<Item> itemList = items.getItem();

		Item item01 = new Item();
		itemList.add(item01);
		Item item02 = new Item();
		itemList.add(item02);

		Coverages coverages = new Coverages();
		region01.setCoverages(coverages);
		List<Coverage> coverageList = coverages.getCoverage();

		Coverage coverage01 = new Coverage();
		RatabaseField input = new RatabaseField();
		input.setName("NPOR_PREMIUM");
		input.setValue("1000");
		coverage01.setInputs(new RatabaseFieldList());
		coverage01.getInputs().getRatabaseField().add(input);
		
		coverage01.setOutputs(new RatabaseFieldList());
		RatabaseField output = new RatabaseField();
		output.setName("PREMIUM");
		output.setValue("2000");
		coverage01.getOutputs().getRatabaseField().add(output );
		coverageList.add(coverage01);
		
		return policy;
	}
}
