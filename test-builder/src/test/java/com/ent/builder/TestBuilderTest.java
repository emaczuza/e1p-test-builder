package com.ent.builder;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import com.ent.builder.TestBuilder;

public class TestBuilderTest {

	@Test
	public void build_test() {
		//String excelFilePath = "C:\\Users\\ref011\\Documents\\SQL\\E1P\\Auto Test Cases\\IL Test Cases.xlsx";
		String excelFilePath = "C:\\Users\\ejm015\\Desktop\\IL Test Cases.xlsx";
		
		//Destination folder must already exist or be empty quotes. Empty quotes will flood workspace
		String folderName = "C:\\Users\\ejm015\\Documents\\SQL\\";
		//String folderName = "testOutput\\"; //viewing in eclipse may require you to refresh project explorer to see results

		try {
			TestBuilder.build(excelFilePath, folderName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertTrue(true);
	}
}
